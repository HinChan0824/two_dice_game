import random

def win_or_lose2(dice1, dice2):

    '''
    Use to check player win, lose or point

    (Integer) dice1 : one of the random number dice
    (Integer) dice2 : second of the random number dice
    '''

    #The sum of dice1 and dice2
    sum_of_dice = dice1 + dice2
    
    #Use to display win ,lose, point message
    win_or_lose = ""
  
    #if player win
    if sum_of_dice in (7, 11): 
        win_or_lose = "Win"
        print("Dice1: {}, Dice2: {}, Sum:{}, {}".format(dice1, dice2, sum_of_dice, win_or_lose))
        print('-' * 50)

    #if player lose
    elif sum_of_dice in (2, 3, 12): 
        win_or_lose = "Lose"
        print("Dice1: {}, Dice2: {}, Sum:{}, {}".format(dice1, dice2, sum_of_dice, win_or_lose))
        print('-' * 50)

    #if player point
    elif sum_of_dice in (4, 5, 6, 8, 9, 10): 
        win_or_lose = "Point"
        print("Dice1: {}, Dice2: {}, Sum:{}, {}".format(dice1, dice2, sum_of_dice, win_or_lose))

        #Continue rolling the dice 
        while win_or_lose != "Lose": #sum is 7 then end loop
            dice1 = random.randint(1, 6)
            dice2 = random.randint(1, 6)

            #Sum again to check if it is make the point
            again_sum_of_dice = dice1 + dice2

            #Player roll the same sum, so win and get out the while loop
            if again_sum_of_dice == sum_of_dice:
                win_or_lose = "Win"
                print("Again: Dice1: {}, Dice2: {}, Sum:{}, Make Your Point: {}".format(dice1, dice2, again_sum_of_dice, win_or_lose))
                print('-' * 50)
                break 
            
            #Player lose and end the while loop
            elif again_sum_of_dice == 7:
                win_or_lose = "Lose"
                print("Again: Dice1: {}, Dice2: {}, Sum:{}, {}".format(dice1, dice2, again_sum_of_dice, win_or_lose))
                print('-' * 50)
                
            #Player point and loop again
            else:
                win_or_lose = "Point"
                print("Again: Dice1: {}, Dice2: {}, Sum:{}, {}".format(dice1, dice2, again_sum_of_dice, win_or_lose))
                
#Loop 10 rounds to play  
for i in range(1, 11):

    #Create two random number dice 
    dice1 = random.randint(1, 6)
    dice2 = random.randint(1, 6)
    
    #start
    print("Round {} >>>>>>>>>>".format(i))
    win_or_lose2(dice1, dice2)